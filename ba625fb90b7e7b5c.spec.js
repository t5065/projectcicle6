const elem = require('./elements').elements;

class ba625fb90b7e7b5c {

    filtroAberto(){
        cy.get(':checkbox').uncheck({force : true})
        cy.get(elem.statusAberto).check({force : true})
        cy.get(elem.aplicarFiltros).contains('ba625fb90b7e7b5c').click()
        cy.wait(2000)
        cy.get(elem.mensagem).should('have.text', 'Filtro aplicado')
    }
    
    export default new ba625fb90b7e7b5c();
